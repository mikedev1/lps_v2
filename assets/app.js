import "bootstrap/dist/js/bootstrap";
import "./bootstrap";
import "bootstrap-icons/font/bootstrap-icons.scss";
import './styles/app.scss';
import './styles/_variables.scss';
import './styles/elements/navbar.scss';
import './scripts/elements/navbar';

